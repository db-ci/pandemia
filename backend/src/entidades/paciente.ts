export interface Paciente {
    nome: string;
    idade: number;
    sexo: string;
    contagio: boolean;
    resultadoteste1?: boolean;
    resultadoteste2?: boolean;
    horario_atendimento: Date;
    horario_saida : Date;
    informacoes_adicionais: string;
}