import mongoose from 'mongoose';
import request from 'supertest';
import * as dbhandler from './dbhandler';
import { PacienteRepositorio } from '../persistencia/pacienteRepositorio';
import { Paciente } from '../entidades/paciente';
import { PacienteModel } from '../persistencia/pacienteModel';
import app from '../app';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Recriar dados de teste no banco antes de cada teste.
 */
//beforeEach(async () => {
//    await seedDatabase();
//});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('Desafio Pandemia web service PacienteController', () => {
    describe('GET /paciente/buscarPaciente/:id unitário', () => {
        test('deve retornar um paciente com o id indicado', async () => {
            //Mock para a função buscarPorId
            const mockPaciente = {
                nome: 'Mia',
                idade: 29,
                sexo: 'F',
                contagio: true,
                resultadoteste1: true,
                resultadoteste2: false,
                horario_atendimento: new Date('August 10, 2020 09:14:18'),
                horario_saida : new Date('August 10, 2020 09:40:12'),
                informacoes_adicionais: 'Tipo de teste: Sorologia'
            };
            const mockBuscarPorId = jest
                .spyOn(PacienteRepositorio, 'buscarPorId')
                .mockResolvedValue(mockPaciente);

            const result = await request(app).get('/paciente/buscarPaciente/id');
            expect(mockBuscarPorId).toHaveBeenCalledWith('id');
            expect(result.status).toBe(200);
            expect(result.body.nome).toEqual(mockPaciente.nome);
            expect(result.body.idade).toEqual(mockPaciente.idade);
            expect(result.body.sexo).toEqual(mockPaciente.sexo);
            expect(result.body.contagio).toEqual(mockPaciente.contagio);
            expect(result.body.resultadoteste1).toEqual(mockPaciente.resultadoteste1);
            expect(result.body.resultadoteste2).toEqual(mockPaciente.resultadoteste2);
            expect(result.body.informacoes_adicionais).toEqual(mockPaciente.informacoes_adicionais);

                
            //Desfazer os mocks
            mockBuscarPorId.mockReset();
    
        });
    });
});
