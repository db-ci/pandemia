import mongoose from 'mongoose';
import * as dbhandler from './dbhandler';
import { UnidadeRepositorio } from  '../persistencia/unidadeRepositorio';
import { Unidade } from '../entidades/unidade';
import { Paciente } from '../entidades/paciente';
import { UnidadeModel } from '../persistencia/unidadeModel';
import { PacienteModel } from '../persistencia/pacienteModel';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;
/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('Atendimento Repositorio', ()=>{
    describe('BuscaUnidade()', ()=>{
        test('Deve retornar uma lista vazia', async ()=>{
            let unidade = await UnidadeRepositorio.buscarUnidade();
            expect(unidade).toEqual([]);
        });
        test('Deve retornar uma lista com tamanho 3', async ()=>{
            await informacaoParaTeste();
            let unidades = await UnidadeRepositorio.buscarUnidade();
            expect(unidades).toHaveLength(3);
        });
    });
    describe('BuscarPorId()', () => {
        test('Deve retornar uma unidade', async () => {
            let unidade = await informacaoParaTeste();
            const unidadeProcurada =  await UnidadeRepositorio.buscarUnidadePorId(unidade[0]);
            expect(unidadeProcurada).toHaveProperty('nome' , 'Posto Eldorado do Sul');
            expect(unidadeProcurada).toHaveProperty('telefone', 5134912307);
            expect(unidadeProcurada).toHaveProperty('email', 'postoEldorado@gmail.com');
            expect(unidadeProcurada).toHaveProperty('endereco.rua', 'Rua Batori José Rodrigues dos Santos');
            expect(unidadeProcurada).toHaveProperty('endereco.numero', 52);
            expect(unidadeProcurada).toHaveProperty('endereco.bairro', 'Centro');
            expect(unidadeProcurada).toHaveProperty('endereco.cidade', 'Eldorado do Sul');
            expect(unidadeProcurada).toHaveProperty('qtd_atendimento', 20);
            expect(unidadeProcurada).toHaveProperty('qtd_resultado_positivo', 15 );
            expect(unidadeProcurada).toHaveProperty('qtd_resultado_negativo', 5);
            expect(unidadeProcurada).toHaveProperty('pacientes');
            expect(unidadeProcurada).toHaveProperty('qtd_paciente', 20);
            expect((unidadeProcurada as any)._id).toEqual(unidade[0]);
        });
        test('Deve apresentar uma exception ao buscar por idInvalido', async () => {
            const idInvalido = 'invalido';
            expect(async()=>{await UnidadeRepositorio.buscarUnidadePorId(idInvalido)}).rejects.toThrow();
        });
    });
    describe('BuscarUnidadePorNome()', () => {
        test('Deve retornar uma lista com tamanho 1', async () => {
            await informacaoParaTesteBuscaPorNome();
            let unidades = await UnidadeRepositorio.buscarUnidadePorNome('Posto Eldorado do Sul');
            expect(unidades).toHaveLength(3);
        });
        test('Deve apresentar uma lista vazia ao buscar por um nome inexistente', async () => {
            let unidade = await UnidadeRepositorio.buscarUnidadePorNome('ÇÇÇ');
            expect(unidade).toEqual([]);
        });
    });
    describe('AtualizarUnidade()', () =>{
        test('Deve apresentar um erro ao tentar atulizar unidade sem passar paciente', async () => {
            const unidadeId = await informacaoParaTeste();
            const novaUnidade = {
                nome: 'Posto Eldorado do Sul',
                telefone: 5134912307,
                email: 'postoEldorado@gmail.com',
                endereco: { rua: 'Rua Batori José Rodrigues dos Santos', numero: 52, bairro: 'Centro', cidade: 'Eldorado do Sul' },
                qtd_atendimento: 20,
                qtd_resultado_positivo: 15,
                qtd_resultado_negativo: 5,
                qtd_paciente: 20,
            };
            expect(async ()=>{await UnidadeRepositorio.atulizarUnidade(unidadeId[0], novaUnidade as any)}).rejects.toThrow();
        });
        test('Deve apresentar um erro ao tentar atulizar unidade sem passar nome', async() => {
            const unidadeId = await informacaoParaTeste();
            const paciente = await criarPaciente();
            const novaUnidade = {
                telefone: 5134912307,
                email: 'postoEldorado@gmail.com',
                endereco: { rua: 'Rua Batori José Rodrigues dos Santos', numero: 52, bairro: 'Centro', cidade: 'Eldorado do Sul' },
                qtd_atendimento: 20,
                qtd_resultado_positivo: 15,
                qtd_resultado_negativo: 5,
                pacientes: [paciente[0]],
                qtd_paciente: 20,
            };
            expect(async ()=>{await UnidadeRepositorio.atulizarUnidade(unidadeId[0], novaUnidade as any)}).rejects.toThrow();
        });
        test('Deve apresentar um erro ao tentar atulizar unidade sem passar telefone', async() => {
            const unidadeId = await informacaoParaTeste();
            const paciente = await criarPaciente();
            const novaUnidade = {
                nome: 'Posto Eldorado do Sul',
                email: 'postoEldorado@gmail.com',
                endereco: { rua: 'Rua Batori José Rodrigues dos Santos', numero: 52, bairro: 'Centro', cidade: 'Eldorado do Sul' },
                qtd_atendimento: 20,
                qtd_resultado_positivo: 15,
                qtd_resultado_negativo: 5,
                pacientes: [paciente[0]],
                qtd_paciente: 20,
            };
            expect(async ()=>{await UnidadeRepositorio.atulizarUnidade(unidadeId[0], novaUnidade as any)}).rejects.toThrow();
        });
        test('Deve apresentar um erro ao tentar atulizar unidade sem passar email', async() => {
            const unidadeId = await informacaoParaTeste();
            const paciente = await criarPaciente();
            const novaUnidade = {
                nome: 'Posto Eldorado do Sul',
                telefone: 5134912307,
                endereco: { rua: 'Rua Batori José Rodrigues dos Santos', numero: 52, bairro: 'Centro', cidade: 'Eldorado do Sul' },
                qtd_atendimento: 20,
                qtd_resultado_positivo: 15,
                qtd_resultado_negativo: 5,
                pacientes: [paciente[0]],
                qtd_paciente: 20,
            };
            expect(async ()=>{await UnidadeRepositorio.atulizarUnidade(unidadeId[0], novaUnidade as any)}).rejects.toThrow();
        });
        test('Deve apresentar um erro ao tentar atulizar unidade sem passar endereço', async() => {
            const unidadeId = await informacaoParaTeste();
            const paciente = await criarPaciente();
            const novaUnidade = {
                nome: 'Posto Eldorado do Sul',
                telefone: 5134912307,
                email: 'postoEldorado@gmail.com',
                qtd_atendimento: 20,
                qtd_resultado_positivo: 15,
                qtd_resultado_negativo: 5,
                pacientes: [paciente[0]],
                qtd_paciente: 20,
            };
            expect(async ()=>{await UnidadeRepositorio.atulizarUnidade(unidadeId[0], novaUnidade as any)}).rejects.toThrow();
        });
        test('Deve apresentar um erro ao tentar atulizar unidade sem passar quantidade atendimento', async() => {
            const unidadeId = await informacaoParaTeste();
            const paciente = await criarPaciente();
            const novaUnidade = {
                nome: 'Posto Eldorado do Sul',
                telefone: 5134912307,
                email: 'postoEldorado@gmail.com',
                endereco: { rua: 'Rua Batori José Rodrigues dos Santos', numero: 52, bairro: 'Centro', cidade: 'Eldorado do Sul' },
                qtd_resultado_positivo: 15,
                qtd_resultado_negativo: 5,
                pacientes: [paciente[0]],
                qtd_paciente: 20,
            };
            expect(async ()=>{await UnidadeRepositorio.atulizarUnidade(unidadeId[0], novaUnidade as any)}).rejects.toThrow();
        });
        test('Deve apresentar um erro ao tentar atulizar unidade sem passar quantidade positivo', async() => {
            const unidadeId = await informacaoParaTeste();
            const paciente = await criarPaciente();
            const novaUnidade = {
                nome: 'Posto Eldorado do Sul',
                telefone: 5134912307,
                email: 'postoEldorado@gmail.com',
                endereco: { rua: 'Rua Batori José Rodrigues dos Santos', numero: 52, bairro: 'Centro', cidade: 'Eldorado do Sul' },
                qtd_atendimento: 20,
                qtd_resultado_negativo: 5,
                pacientes: [paciente[0]],
                qtd_paciente: 20,
            };
            expect(async ()=>{await UnidadeRepositorio.atulizarUnidade(unidadeId[0], novaUnidade as any)}).rejects.toThrow();
        });
        test('Deve apresentar um erro ao tentar atulizar unidade sem passar quantidade negativo', async() => {
            const unidadeId = await informacaoParaTeste();
            const paciente = await criarPaciente();
            const novaUnidade = {
                nome: 'Posto Eldorado do Sul',
                telefone: 5134912307,
                email: 'postoEldorado@gmail.com',
                endereco: { rua: 'Rua Batori José Rodrigues dos Santos', numero: 52, bairro: 'Centro', cidade: 'Eldorado do Sul' },
                qtd_atendimento: 20,
                qtd_resultado_positivo: 15,
                pacientes: [paciente[0]],
                qtd_paciente: 20,
            };
            expect(async ()=>{await UnidadeRepositorio.atulizarUnidade(unidadeId[0], novaUnidade as any)}).rejects.toThrow();
        });
        test('Deve apresentar um erro ao tentar atulizar unidade sem passar quantidade paciente', async() => {
            const unidadeId = await informacaoParaTeste();
            const paciente = await criarPaciente();
            const novaUnidade = {
                nome: 'Posto Eldorado do Sul',
                telefone: 5134912307,
                email: 'postoEldorado@gmail.com',
                endereco: { rua: 'Rua Batori José Rodrigues dos Santos', numero: 52, bairro: 'Centro', cidade: 'Eldorado do Sul' },
                qtd_atendimento: 20,
                qtd_resultado_positivo: 15,
                qtd_resultado_negativo: 5,
                pacientes: [paciente[0]],
            };
            expect(async ()=>{await UnidadeRepositorio.atulizarUnidade(unidadeId[0], novaUnidade as any)}).rejects.toThrow();
        });
        test('Deve retornar uma unidade de saude alterado', async () => {
            const unidadeId = await informacaoParaTeste();
            const paciente = await criarPaciente();
            const novaUnidade:Unidade = await UnidadeModel.create({
                nome: 'Posto de Saúde Bairro Chácara',
                telefone: 51334996415,
                email: 'postoDeSaudeBairroChacara@gmail.com',
                endereco: { rua: 'Av. Nestor Jardim Filho', numero: 1174, bairro: 'Chácara', cidade: 'Eldorado do Sul' },
                qtd_atendimento: 50,
                qtd_resultado_positivo: 32,
                qtd_resultado_negativo: 18,
                pacientes: [paciente[0]],
                qtd_paciente: 50,
            });
            const unidadeAlterada = await UnidadeRepositorio.atulizarUnidade(unidadeId[0], novaUnidade);
            expect(unidadeAlterada.nome).toEqual(novaUnidade.nome);
            expect(unidadeAlterada.telefone).toEqual(novaUnidade.telefone);
            expect(unidadeAlterada.email).toEqual(novaUnidade.email);
            expect(unidadeAlterada.endereco.bairro).toEqual(novaUnidade.endereco.bairro);
            expect(unidadeAlterada.endereco.cidade).toEqual(novaUnidade.endereco.cidade);
            expect(unidadeAlterada.endereco.numero).toEqual(novaUnidade.endereco.numero);
            expect(unidadeAlterada.endereco.rua).toEqual(novaUnidade.endereco.rua);
            expect(unidadeAlterada.qtd_atendimento).toEqual(novaUnidade.qtd_atendimento);
            expect(unidadeAlterada.qtd_resultado_positivo).toEqual(novaUnidade.qtd_resultado_positivo);
            expect(unidadeAlterada.qtd_resultado_negativo).toEqual(novaUnidade.qtd_resultado_negativo);
            expect(unidadeAlterada.pacientes[0]).toEqual(novaUnidade.pacientes[0]);
            expect(unidadeAlterada.qtd_paciente).toEqual(novaUnidade.qtd_paciente);
        });
        test('Deve apresentar uma exception ao alterar passando idInvalido', async () => {
            const idInvalido = 'invalido';
            const unidade = await criarUnidade();
            expect(async()=>{await UnidadeRepositorio.atulizarUnidade(idInvalido, unidade)}).rejects.toThrow();
        });
    });
    describe('Excluir()', () => {
        test('Deve apresentar uma exception ao excluir passando idInvalido', async () => {
            const idInvalido = 'invalido';
            expect(async()=>{await UnidadeRepositorio.excluirUnidade(idInvalido)}).rejects.toThrow();
        });
        test('Deve remover item do banco de dados', async () => {
            const unidadeId = await informacaoParaTeste();
            const remove = await UnidadeRepositorio.excluirUnidade(unidadeId[0]);
            expect(await UnidadeRepositorio.buscarUnidade()).toHaveLength(2);
        });
        test('Deve retornar unidade removida', async () => {
            const unidadeId = await informacaoParaTeste();
            const remove = await UnidadeRepositorio.excluirUnidade(unidadeId[0]);
            expect((remove as any)._id).toEqual(unidadeId[0]);
        });
    });
    describe('AdicionarUnidade()', () => {
        test('Deve apresentar uma exception ao incluir sem paciente', async () => {
            let unidade = await criarUnidade();
            delete unidade.pacientes[0];
            expect(async()=>{await UnidadeRepositorio.adicionarUnidade(unidade)}).rejects.toThrow();
        });
        test('Deve apresentar uma exception ao incluir sem nome', async () =>{
            let unidade = await criarUnidade();
            delete unidade.nome;
            expect(async()=>{await UnidadeRepositorio.adicionarUnidade(unidade)}).rejects.toThrow();
        });
        test('Deve apresentar uma exception ao incluir sem email', async () =>{
            let unidade = await criarUnidade();
            delete unidade.email;
            expect(async()=>{await UnidadeRepositorio.adicionarUnidade(unidade)}).rejects.toThrow();
        });
        test('Deve apresentar uma exception ao incluir sem quantidadeAtendimento', async () =>{
            let unidade = await criarUnidade();
            delete unidade.qtd_atendimento;
            expect(async()=>{await UnidadeRepositorio.adicionarUnidade(unidade)}).rejects.toThrow();
        });
        test('Deve apresentar uma exception ao incluir sem quantidade resultado positivo', async () =>{
            let unidade = await criarUnidade();
            delete unidade.qtd_resultado_positivo;
            expect(async()=>{await UnidadeRepositorio.adicionarUnidade(unidade)}).rejects.toThrow();
        });
        test('Deve apresentar uma exception ao incluir sem quantidade resultado negativo', async () =>{
            let unidade = await criarUnidade();
            delete unidade.qtd_resultado_negativo;
            expect(async()=>{await UnidadeRepositorio.adicionarUnidade(unidade)}).rejects.toThrow();
        });
        test('Deve apresentar uma exception ao incluir sem quantidade paciente', async () =>{
            let unidade = await criarUnidade();
            delete unidade.qtd_paciente;
            expect(async()=>{await UnidadeRepositorio.adicionarUnidade(unidade)}).rejects.toThrow();
        });
        test('Deve apresentar uma exception ao incluir sem quantidade endereço', async () =>{
            let unidade = await criarUnidade();
            delete unidade.endereco.bairro;
            delete unidade.endereco.cidade;
            delete unidade.endereco.numero;
            delete unidade.endereco.rua;
            expect(async()=>{await UnidadeRepositorio.adicionarUnidade(unidade)}).rejects.toThrow();
        });
        test('Deve retornar uma unidade cadastrada', async() => {
            let unidade = await criarUnidade();
            const unidadeInserida = await UnidadeRepositorio.adicionarUnidade(unidade);
            expect(unidadeInserida).toHaveProperty('_id');
            expect(unidadeInserida.nome).toEqual(unidadeInserida.nome);
            expect(unidadeInserida.telefone).toEqual(unidadeInserida.telefone);
            expect(unidadeInserida.email).toEqual(unidadeInserida.email);
            expect(unidadeInserida.endereco).toEqual(unidadeInserida.endereco);
            expect(unidadeInserida.qtd_atendimento).toEqual(unidadeInserida.qtd_atendimento);
            expect(unidadeInserida.qtd_resultado_positivo).toEqual(unidadeInserida.qtd_resultado_positivo);
            expect(unidadeInserida.qtd_resultado_negativo).toEqual(unidadeInserida.qtd_resultado_negativo);
            expect(unidade.pacientes[0]).toEqual(unidadeInserida.pacientes[0]);
            expect(unidadeInserida.qtd_paciente).toEqual(unidadeInserida.qtd_paciente);
        });
    });
});


/*
    Valores que serão adicionadas no banco para os testes.
*/
async function informacaoParaTeste():Promise<string[]>{
    let unidade1 = await UnidadeModel.create(await criarUnidade());
    let unidade2 = await UnidadeModel.create(await criarUnidade());
    let unidade3 = await UnidadeModel.create(await criarUnidade());
    return [unidade1._id, unidade2._id, unidade3._id];

}

/*
    Valores que serão adicionadas no banco para os testes de buscar informação unidadePorNome().
*/
async function informacaoParaTesteBuscaPorNome():Promise<string[]>{
    let unidade1 = await UnidadeModel.create(await criarUnidade());
    let unidade2 = await UnidadeModel.create(await criarUnidade());
    let unidade3 = await UnidadeModel.create(await criarUnidade());
    return [unidade1.nome, unidade2.nome, unidade3.nome];
}


     /*
        seta os valores para criar uma unidade
     */

    async function criarUnidade():Promise<Unidade>{
        const paciente: Paciente[] = await criarPaciente();
        return {
            nome: 'Posto Eldorado do Sul',
            telefone: 5134912307,
            email: 'postoEldorado@gmail.com',
            endereco: { rua: 'Rua Batori José Rodrigues dos Santos', numero: 52, bairro: 'Centro', cidade: 'Eldorado do Sul' },
            qtd_atendimento: 20,
            qtd_resultado_positivo: 15,
            qtd_resultado_negativo: 5,
            pacientes: [paciente[0]],
            qtd_paciente: 20,
        }
    }

    /*
        Informações setadas do paciente
    */
    async function informacoesDoPaciente(): Promise<Paciente>{
        return {
            nome: 'Camila',
            idade: 23,
            sexo: 'Feminino',
            contagio: false,
            horario_atendimento: new Date('August 21, 2020 16:49:30'),
            horario_saida: new Date('August 21, 2020 16:49:30'),
            resultadoteste1: false,
            resultadoteste2: false,
           informacoes_adicionais: 'O paciente não apresenta nenhum sintoma, não foi necessario fazer os testes'
        };
    }
 
     /*
       Cria um paciente
    */
    async function criarPaciente(): Promise<Paciente[]>{
        let paciente1 = await PacienteModel.create( await informacoesDoPaciente());
        let paciente2 = await PacienteModel.create(await informacoesDoPaciente());
        return [paciente1, paciente2];
    }

