import mongoose from 'mongoose';
import request from 'supertest';
import * as dbhandler from './dbhandler';
import { UnidadeRepositorio } from '../persistencia/unidadeRepositorio';
import { Unidade } from '../entidades/unidade';
import { UnidadeModel } from '../persistencia/unidadeModel';
import app from '../app';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000


/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Recriar dados de teste no banco antes de cada teste.
 */
//beforeEach(async () => {
//    await seedDatabase();
//});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('Desafio Pandemia web service UnidadeController', () => {
    describe('GET /unidade/buscarUnidade/:id unitário', () => {
        test('deve retornar uma unidade de saúde com o id indicado', async () => {
            //Mock para a função buscarUnidadePorId
            const mockUnidade: Unidade = {
                nome: 'Posto Eldorado do Sul',
                telefone: 5134912307,
                email: 'postoEldorado@gmail.com',
                endereco: { rua: 'Rua Batori José Rodrigues dos Santos', numero: 52, bairro: 'Centro', cidade: 'Eldorado do Sul' },
                qtd_atendimento: 20,
                qtd_resultado_positivo: 15,
                qtd_resultado_negativo: 5,
                pacientes: [],
                qtd_paciente: 20
            };
            const mockBuscaUnidadePorId = jest
                .spyOn(UnidadeRepositorio, 'buscarUnidadePorId')
                .mockResolvedValue(mockUnidade);

            const result = await request(app).get('/unidade/buscarUnidade/id');
            expect(mockBuscaUnidadePorId).toHaveBeenCalledWith('id');
            expect(result.status).toBe(200);
            expect(result.body.nome).toEqual(mockUnidade.nome);
            expect(result.body.telefone).toEqual(mockUnidade.telefone);
            expect(result.body.email).toEqual(mockUnidade.email);
            expect(result.body.qtd_atendimento).toEqual(mockUnidade.qtd_atendimento);
            expect(result.body.qtd_resultado_positivo).toEqual(mockUnidade.qtd_resultado_positivo);
            expect(result.body.qtd_resultado_negativo).toEqual(mockUnidade.qtd_resultado_negativo);
            expect(result.body.qtd_paciente).toEqual(mockUnidade.qtd_paciente);

                
            //Desfazer os mocks
            mockBuscaUnidadePorId.mockReset();
    
        });
    });
});
