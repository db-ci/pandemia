import mongoose from 'mongoose';
import * as dbhandler from './dbhandler';
import { PacienteRepositorio } from '../persistencia/pacienteRepositorio';
import { Paciente } from '../entidades/paciente';
import { PacienteModel } from '../persistencia/pacienteModel';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('PacienteRepositorio', () => {
    const mockPaciente = {
        nome: 'Mia',
        idade: 29,
        sexo: 'F',
        contagio: true,
        resultadoteste1: true,
        resultadoteste2: false,
        horario_atendimento: new Date('August 10, 2020 09:14:18'),
        horario_saida : new Date('August 10, 2020 09:40:12'),
        informacoes_adicionais: 'Tipo de teste: Sorologia'
    };

    const mockPacienteNomeErro = {
        nome: '',
        idade: 29,
        sexo: 'F',
        contagio: true,
        resultadoteste1: true,
        resultadoteste2: false,
        horario_atendimento: new Date('August 10, 2020 09:14:18'),
        horario_saida : new Date('August 10, 2020 09:40:12'),
        informacoes_adicionais: 'Tipo de teste: Sorologia'
    };

    describe('criar()', () => {
        test('deve inserir um paciente', async () => {
            const res =  await PacienteRepositorio.criar(mockPaciente);
            expect(res.nome).toEqual(mockPaciente.nome);
            expect(res.idade).toEqual(mockPaciente.idade);
            expect(res.sexo).toEqual(mockPaciente.sexo);
            expect(res.contagio).toEqual(mockPaciente.contagio);
            expect(res.resultadoteste1).toEqual(mockPaciente.resultadoteste1);
            expect(res.resultadoteste2).toEqual(mockPaciente.resultadoteste2);
            expect(res.horario_atendimento).toEqual(mockPaciente.horario_atendimento);
            expect(res.horario_saida).toEqual(mockPaciente.horario_saida);
            expect(res.informacoes_adicionais).toEqual(mockPaciente.informacoes_adicionais);
        });

        test('requer paciente com nome válido', async () => {
            await expect(PacienteRepositorio.criar(mockPacienteNomeErro))
                  .rejects
                  .toThrow(mongoose.Error.ValidationError);
        });
    });

    describe('buscar()', () => {
        test('deve retornar um paciente vazio', async () => {
            const resultPaciente =  await PacienteRepositorio.buscar();
            expect(resultPaciente).toBeDefined();
            expect(resultPaciente).toHaveLength(0);
        });

        test('deve retornar todos os pacientes', async() => {
            await seedDatabase();
            const resultPacientes = await PacienteRepositorio.buscar();
            expect(resultPacientes).toBeDefined();
            expect(resultPacientes).toHaveLength(4);
            expect(resultPacientes[0].nome).toEqual('Yasmim');
            expect(resultPacientes[1].nome).toEqual('Leon');
            expect(resultPacientes[2].nome).toEqual('Jeniffer');
            expect(resultPacientes[3].nome).toEqual('Lily');
        });
    });

    describe('buscarPorId()', () => {
        test('deve buscar um paciente pelo id', async () => {
            const mockPacienteRepositorioBuscarPorId = jest
                .spyOn(PacienteRepositorio, 'buscarPorId')
                .mockResolvedValue(mockPaciente);

            const resultPacienteId = await PacienteRepositorio.buscarPorId('id');
            expect(resultPacienteId).toBeDefined(); 
            expect(mockPacienteRepositorioBuscarPorId).toHaveBeenCalledWith('id');
        });

        test('requer um id válido', async () => { 
            const mockPacienteRepositorioBuscarPorId  = jest
            .fn()
            .mockRejectedValue(new Error('Async error'));

            const erroIdPaciente = await PacienteRepositorio.buscarPorId('id');
            expect(erroIdPaciente).toBeDefined(); 
            })
        });

    describe('alterarPaciente()', () => {
        test('deve alterar um paciente', async () => {
            const mockPacienteRepositorioAlterarPaciente = jest
                .spyOn(PacienteRepositorio, 'alterarPaciente')
                .mockResolvedValue(mockPaciente);

            const alteraPaciente = await PacienteRepositorio.alterarPaciente(mockPaciente, 'id');
            expect(mockPaciente).toEqual(alteraPaciente);
            expect(alteraPaciente).toBeDefined(); 
        });

        test('deve alterar o paciente com um nome válido', async () => { 
            const mockPacienteRepositorioAlterarPaciente = jest
            .fn()
            .mockRejectedValue(new Error('Async error'));

            const erroPaciente = await PacienteRepositorio.alterarPaciente(mockPacienteNomeErro, 'id');
            expect(erroPaciente).toBeDefined(); 
            })
        });

    describe('deletarPaciente()', () => {
        test('deve deletar um paciente', async () => {
            expect(async () =>{
                const deletaPaciente = await PacienteRepositorio.deletarPaciente('id');
                expect(deletaPaciente).toBeDefined(); 
        });
    });

        test('deve deletar um paciente válido', async () => {
            expect(async () =>{
                const deletaPaciente = await PacienteRepositorio.deletarPaciente('id')
            }).rejects.toThrowError();
        });
    });

async function seedDatabase() {
    const paciente1 = {
        nome: 'Yasmim',
        idade: 42,
        sexo: 'F',
        contagio: false,
        resultadoteste1: false,
        resultadoteste2: false,
        horario_atendimento: new Date('August 10, 2020 16:54:30'),
        horario_saida : new Date('August 10, 2020 17:25:30'),
        informacoes_adicionais: 'Tipo de teste: Teste Rápido IgG e IgM'
    };
    const paciente2 = {
        nome: 'Leon',
        idade: 35,
        sexo: 'M',
        contagio: true,
        resultadoteste1: true,
        resultadoteste2: true,
        horario_atendimento: new Date('August 10, 2020 11:36:30'),
        horario_saida : new Date('August 10, 2020 12:15:23'),
        informacoes_adicionais: 'Tipo de teste: Sorologia e RT-PCR'
    };

    const paciente3 = {
        nome: 'Jeniffer',
        idade: 27,
        sexo: 'F',
        contagio: true,
        resultadoteste1: true,
        resultadoteste2: true,
        horario_atendimento: new Date('July 30, 2020 17:38:37'),
        horario_saida : new Date('July 30, 2020 18:10:03'),
        informacoes_adicionais: 'Tipo de teste: RT-PCR'
    };

    const paciente4 = {
        nome: 'Lily',
        idade: 18,
        sexo: 'F',
        contagio: false,
        resultadoteste1: false,
        resultadoteste2: false,
        horario_atendimento: new Date('July 3, 2020 13:45:24'),
        horario_saida : new Date('July 30, 2020 14:18:53'),
        informacoes_adicionais: 'Tipo de teste: RT-PCR'
    };

    await PacienteRepositorio.criar(paciente1);
    await PacienteRepositorio.criar(paciente2);
    await PacienteRepositorio.criar(paciente3);
    await PacienteRepositorio.criar(paciente4);
    }
});