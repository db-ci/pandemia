import express from 'express';
import errorHandler from 'errorhandler';
import helmet from 'helmet';
import cors from 'cors';
import bodyParser from 'body-parser';
import { unidadeRouter } from './controladores/unidade.route';
import { pacienteRouter } from './controladores/paciente.route';

// Configuração middleware do Express
const app = express();
app.set('port', process.env.PORT);
app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
if (process.env.NODE_ENV === 'development' ){
    app.use(errorHandler());
} 

// Configuração das rotas
app.use(unidadeRouter);
app.use(pacienteRouter);

export default app;
