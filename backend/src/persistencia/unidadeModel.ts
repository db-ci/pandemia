import { Document, model, Schema, SchemaTypes, Model } from 'mongoose';
import { Unidade } from "../entidades/unidade";

interface UnidadeDocument extends Document, Unidade { }

const EnderecoSchema = new Schema({
    rua: {type: String, required: true, minlength: 1, maxlength: 100},
    numero: {type: Number, required: true, minlength: 1, maxlength: 10},
    bairro: {type: String, required: true, minlength: 1, maxlength: 100},
    cidade: {type: String, required: true, minlength: 1, maxlength: 100}
}); 

const UnidadeSchema = new Schema({
    nome: { type: String, required: true, max: 100 },
    telefone: { type: Number, required: true, minlength: 8, maxlength: 11 },
    email: { type: String, required: true, max: 100 },
    endereco: { type: EnderecoSchema },
    qtd_atendimento: { type: Number, required: true, max: 100 },
    qtd_resultado_positivo: { type: Number, required: true, max: 100 },
    qtd_resultado_negativo: { type: Number, required: true, max: 100 },
    pacientes: [{ type: SchemaTypes.ObjectId, ref: 'Paciente', required: true }],
    qtd_paciente: { type: Number, required: true, max: 100 }
});

export const UnidadeModel: Model<UnidadeDocument> = model<UnidadeDocument>('Unidade', UnidadeSchema, 'unidades');