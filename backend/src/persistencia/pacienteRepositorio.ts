import { Paciente } from "../entidades/paciente";
import { PacienteModel } from "./pacienteModel";

export class PacienteRepositorio {
    static async criar(paciente: Paciente): Promise<Paciente> { 
        return PacienteModel.create(paciente); 
    }

    static async buscar(): Promise<Paciente[]> {
        let consulta = PacienteModel.find();
        return consulta.exec(); 
    }

    static async buscarPorId(id: string): Promise<Paciente | null> {
        let consulta = PacienteModel.findById(id).exec();
        if (consulta != null) {
            return consulta; 
        } else {
            throw new Error('Id inexistente'); 
        }
    }

    static async alterarPaciente(paciente: Paciente, id: string): Promise<Paciente> { //c
        let pacienteAtual = await PacienteModel.findById(id).exec();
        if (pacienteAtual !== null) {
            pacienteAtual.nome = paciente.nome;
            pacienteAtual.idade = paciente.idade;
            pacienteAtual.sexo = paciente.sexo;
            pacienteAtual.contagio = paciente.contagio;
            pacienteAtual.resultadoteste1 = paciente.resultadoteste1;
            pacienteAtual.resultadoteste2 = paciente.resultadoteste2;
            pacienteAtual.horario_atendimento = paciente.horario_atendimento;
            pacienteAtual.horario_saida = paciente.horario_saida;
            pacienteAtual.informacoes_adicionais = paciente.informacoes_adicionais;
            return pacienteAtual.save();
        } else {
            throw new Error('Id inexistente'); 
        }
    }

    static async deletarPaciente (id: string): Promise<Paciente>{
        let resposta = await PacienteModel.findById(id).exec();
        if(resposta  != null){
            return resposta.remove();
        } else {
         throw new Error('Id inexistente');
        }
    }
}
