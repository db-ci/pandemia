import { Paciente } from "../entidades/paciente";
import { Document, model, Schema, Model } from "mongoose";

interface PacienteDocument extends Document, Paciente {}

const PacienteSchema = new Schema({
    nome: { type: String, required: true,  max: 100  },
    idade: { type: Number, required: true,  max: 100  },
    sexo: { type: String, required: true,  max: 100  },
    contagio: { type: Boolean, required: true,  max: 100 },
    resultadoteste1: { type: Boolean, required: false,  max: 100 },
    resultadoteste2: { type: Boolean, required: false,  max: 100 },
    horario_atendimento: { type: Date, required: true },
    horario_saida : { type: Date, required: true },
    informacoes_adicionais: { type: String, required: true,  max: 100 },
});

export const PacienteModel: Model<PacienteDocument> = model<PacienteDocument>('Paciente', PacienteSchema, 'pacientes');