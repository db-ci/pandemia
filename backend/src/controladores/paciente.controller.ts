import { Request, Response, NextFunction } from 'express';
import { Paciente } from '../entidades/paciente';
import { PacienteRepositorio } from '../persistencia/pacienteRepositorio';


export async function adicionaPaciente(req: Request, res: Response, next: NextFunction) {
    try {
        const paciente = req.body as Paciente;
        await PacienteRepositorio.criar(paciente);
        res.status(201).send('Paciente adicionado com sucesso.');     
    } catch (error) {
        next(error);
    }
}

export async function buscaPacientes(req: Request, res: Response, next: NextFunction) {
    try {
        const pacientes = await PacienteRepositorio.buscar();
        res.status(200).json(pacientes);        
    } catch (error) {
        next(error);
    }
}

export async function buscaPacientePorId(req: Request, res: Response, next: NextFunction) {
    try {
        const id = req.params.id;
        const paciente = await PacienteRepositorio.buscarPorId(id);
        if (paciente !== null) {
            res.status(200).json(paciente);         
        } else {
            res.sendStatus(404);    
        }
    } catch (error) {
        next(error);
    }
}

export async function atualizaPaciente(req: Request, res: Response, next: NextFunction) {
    try {
        const id = req.params.id;
        const pacienteNovo = req.body as Paciente;
        if (pacienteNovo !== null) {
            await PacienteRepositorio.alterarPaciente(pacienteNovo, id);
            res.status(200).send('Paciente atualizado com sucesso.');         
        } else {
            res.sendStatus(404);   
        }
    } catch (error) {
        next(error);
    }
}

export async function deletaPaciente(req: Request, res: Response, next: NextFunction) {
    try {
        const id = req.params.id;
        await PacienteRepositorio.deletarPaciente(id);
        res.status(200).send('Paciente deletado com sucesso.');     
    } catch (error) {
        next(error);
    }
}
