import { Request, Response, NextFunction } from 'express';
import { Unidade } from '../entidades/unidade';
import { UnidadeRepositorio } from '../persistencia/unidadeRepositorio';


export async function adicionaUnidade(req: Request, res: Response, next: NextFunction) {
    try {
        const unidade = req.body as Unidade;
        await UnidadeRepositorio.adicionarUnidade(unidade);
        res.status(201).send('Unidade adicionada com sucesso.');        // CREATED
    } catch (error) {
        next(error);
    }
}

export async function buscaUnidades(req: Request, res: Response, next: NextFunction) {
    try {
        const unidades: Unidade[] = await UnidadeRepositorio.buscarUnidade();
        res.status(200).json(unidades);    // ARRAY?
    } catch (error) {
        next(error);
    }
}

export async function buscaUnidadePorId(req: Request, res: Response, next: NextFunction) {
    try {
        const id = req.params.id;
        const unidade = await UnidadeRepositorio.buscarUnidadePorId(id);
        if (unidade !== null) {
            res.status(200).json(unidade);          // OK
        } else {
            res.sendStatus(404);    // not found
        }
    } catch (error) {
        next(error);
    }
}

export async function buscaUnidadePorNome(req: Request, res: Response, next: NextFunction) {
    try {
        const nome = req.params.nome;
        const unidade = await UnidadeRepositorio.buscarUnidadePorNome(nome);
        if (unidade !== null) {
            res.status(200).json(unidade);          // OK
        } else {
            res.sendStatus(404);    // not found
        }
    } catch (error) {
        next(error);
    }
}

export async function atualizaUnidade(req: Request, res: Response, next: NextFunction) {
    try {
        const id = req.params.id;
        const unidadeNova = req.body as Unidade;
        if (unidadeNova !== null) {
            await UnidadeRepositorio.atulizarUnidade(id, unidadeNova);
            res.status(200).send('Unidade atualizada com sucesso.');          // OK
        } else {
            res.sendStatus(404);    // not found
        }
    } catch (error) {
        next(error);
    }

}

export async function deletaUnidade(req: Request, res: Response, next: NextFunction) {
    try {
        const id = req.params.id;
        await UnidadeRepositorio.excluirUnidade(id);
        res.status(200).send('Unidade deletada com sucesso.');      // OK
    } catch (error) {
        next(error);
    }
}
