import { Router } from 'express';
import * as pacienteController from './paciente.controller';

export const pacienteRouter = Router();
export const path = '/paciente';

pacienteRouter.post(`${path}/adicionarPaciente`, pacienteController.adicionaPaciente);      // cadastrar novo paciente
pacienteRouter.get(`${path}/buscarPacientes`, pacienteController.buscaPacientes);       // buscar pacientes
pacienteRouter.get(`${path}/buscarPaciente/:id`, pacienteController.buscaPacientePorId);       // buscar paciente por id
pacienteRouter.patch(`${path}/atualizarPaciente/:id`, pacienteController.atualizaPaciente);     // atualizar dados de um paciente
pacienteRouter.delete(`${path}/deletarPaciente/:id`, pacienteController.deletaPaciente);  /// deletar paciente

