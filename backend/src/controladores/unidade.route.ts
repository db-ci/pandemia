import { Router } from 'express';
import * as unidadeController from './unidade.controller';

export const unidadeRouter = Router();
export const path = '/unidade';

unidadeRouter.post(`${path}/adicionarUnidade`, unidadeController.adicionaUnidade);      // cadastrar nova unidade
unidadeRouter.get(`${path}/buscarUnidades`, unidadeController.buscaUnidades);       // buscar unidades
unidadeRouter.get(`${path}/buscarUnidade/:id`, unidadeController.buscaUnidadePorId);       // buscar unidade por id
unidadeRouter.get(`${path}/buscarUnidadeNome/:nome`, unidadeController.buscaUnidadePorNome);       // buscar unidade por id
unidadeRouter.patch(`${path}/atualizarUnidade/:id`, unidadeController.atualizaUnidade);     // atualizar dados de uma unidade
unidadeRouter.delete(`${path}/deletarUnidade/:id`, unidadeController.deletaUnidade);    // deletar unidade 
