import { connect } from 'mongoose';
import app from './app';

async function main() {
    try {
        const url = `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DATABASE}`;
        const cliente = await connect (url, {useNewUrlParser: true, useUnifiedTopology: true});
        console.log('Conectado com sucesso ao banco');
        app.listen(app.get('port'), () => {
            console.log(`Express executando na porta: ${app.get('port')}`);
            console.log(`Express no modo: ${app.get('env')}`);
        });
    } catch (error) {
        console.log(`Erro: ${error}`);
    }
}

main();
