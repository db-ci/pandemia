import { Paciente } from './paciente';

export interface Unidade {
    nome: string;
    telefone: number;
    email: string;
    endereco: {
        rua: string;
        numero: number;
        bairro: string;
        cidade: string;
    };
    qtd_atendimento: number;
    qtd_resultado_positivo: number;
    qtd_resultado_negativo: number;
    pacientes: Paciente[];
    qtd_paciente: number;
}

