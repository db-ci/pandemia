import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Paciente } from '../../entidades/paciente';
import { UnidadeService } from '../unidade.service';
import { PacienteService } from '../paciente.service';


@Component({
  selector: 'app-cadastro-unidade',
  templateUrl: './cadastro-unidade.component.html',
  styleUrls: ['./cadastro-unidade.component.css']
})
export class CadastroUnidadeComponent implements OnInit {
  checkoutForm: FormGroup;
  checkoutFormAdress: FormGroup;
  paciente: Paciente[];

  constructor(private unidade: UnidadeService, private pacienteService: PacienteService, private formBuilder: FormBuilder) {
    this.checkoutForm = this.formBuilder.group({
    nome:  ['', Validators.required],
    telefone: ['', Validators.required],
    email: ['', Validators.required],
    qtd_atendimento: ['', Validators.maxLength],
    qtd_resultado_positivo: ['', Validators.maxLength],
    qtd_resultado_negativo: ['', Validators.maxLength],
    pacientes: ['', ],
    qtd_paciente: ['', Validators.maxLength],
    });
    this.checkoutFormAdress = this.formBuilder.group({
      rua: ['', Validators.required ],
      numero: ['', Validators.required],
      bairro: ['', Validators.required],
      cidade: ['', Validators.required],
    });

  }

  ngOnInit(): void {
    this.pacienteService.getPacientes().subscribe({next: pacientes => this.paciente = pacientes});
  }

  get pacienteId(){
    return this.checkoutForm.get('pacientes');
  }

  onSubmit(customerData, customerDataAdress) {
    this.unidade.postUnidade({
      nome: customerData.nome,
      telefone: customerData.telefone,
      email: customerData.email,
      endereco: {
        rua: customerDataAdress.rua,
        numero: customerDataAdress.numero,
        bairro: customerDataAdress.bairro,
        cidade: customerDataAdress.cidade,
      },
      qtd_atendimento: customerData.qtd_atendimento,
      qtd_resultado_positivo: customerData.qtd_resultado_positivo,
      qtd_resultado_negativo: customerData.qtd_resultado_negativo,
      pacientes: this.pacienteId.value,
      qtd_paciente: customerData.qtd_paciente,
     }).subscribe({
      next: console.log,
      error: console.warn
    });
    this.checkoutForm.reset();
    this.checkoutFormAdress.reset();
  }
}
