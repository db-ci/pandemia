import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { GoogleChartsModule } from 'angular-google-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadastroUnidadeComponent } from './cadastro-unidade/cadastro-unidade.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { UnityMainComponent } from './unity-main/unity-main.component';
import { HealthDataComponent } from './health-data/health-data.component';
import { CadastroAtendimentoComponent } from './cadastro-atendimento/cadastro-atendimento.component';
import { ViewUnityComponent } from './view-unity/view-unity.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    CadastroUnidadeComponent,
    UnityMainComponent,
    CadastroAtendimentoComponent,
    ViewUnityComponent,
    HealthDataComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSelectModule,
    MatTableModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    FormsModule,
    GoogleChartsModule,
    NgxChartsModule,
    RouterModule.forRoot([
      {path: '', component:  UnityMainComponent, data: {title: 'Home'}},
      {path: 'data', component: HealthDataComponent, data: {title: 'Dados gerais'}},
      {path: 'cadastroUnidade', component: CadastroUnidadeComponent, data: {title: 'Cadastro Unidade'}},
      {path: 'visalizarUnidade', component: ViewUnityComponent, data: {title: 'Visualizar Unidade'}},
      {path: 'cadastroAtendimento', component: CadastroAtendimentoComponent, data: {title: 'CadastroAtendimento'}},
      {path: 'login', component: LoginComponent, data: {title: 'Login'}},

    ]),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
