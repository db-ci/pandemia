import { Component, OnInit } from '@angular/core';
import { Unidade } from '../../entidades/unidade';
import { UnidadeService } from '../unidade.service';

@Component({
  selector: 'app-view-unity',
  templateUrl: './view-unity.component.html',
  styleUrls: ['./view-unity.component.css']
})
export class ViewUnityComponent implements OnInit {
  unidade: Unidade[];
  unidadeCarregada: Unidade[];
  selectedUnidade: string;
  displayedColumns = ['nome', 'quantidade_Paciente', 'qtd_atendimento', 'qtd_resultado_positivo', 'qtd_resultado_negativo'];

  constructor(private unidadeService: UnidadeService) { }

  ngOnInit(): void {
    this.unidadeService.getUnidades().subscribe(unidade => this.unidade = unidade);
    this.selectedUnidade = this.unidade[1].nome;
  }

  getSelectedUnity(event){
    this.unidadeService.getUnidadesPorNome(event.value).subscribe(unidade => this.unidadeCarregada = unidade);
  }
}
