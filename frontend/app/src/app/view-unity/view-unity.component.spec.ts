import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUnityComponent } from './view-unity.component';

describe('ViewUnityComponent', () => {
  let component: ViewUnityComponent;
  let fixture: ComponentFixture<ViewUnityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUnityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUnityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
