import { Component, OnInit } from '@angular/core';
import {Routes, Route, Router, RoutesRecognized} from '@angular/router';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {

  routes: Route[];
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    this.routes = this.router.config;
    console.log(this.routes);
  }


}
