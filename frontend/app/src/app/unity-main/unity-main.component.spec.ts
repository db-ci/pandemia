import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnityMainComponent } from './unity-main.component';

describe('UnityMainComponent', () => {
  let component: UnityMainComponent;
  let fixture: ComponentFixture<UnityMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnityMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnityMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
