import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-unity-main',
  templateUrl: './unity-main.component.html',
  styleUrls: ['./unity-main.component.css']
})
export class UnityMainComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  
  imagePath = '/assets/virus.jpg';
}
