import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Paciente } from '../entidades/paciente';
import { Observable, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  constructor(
    public http: HttpClient
  ) { }

  postPaciente(user:Paciente): Observable<Paciente>{
    return this.http.post<Paciente>(environment.URLBASE + '/paciente/adicionarPaciente', user).pipe(
      catchError(this.errorHandler<Paciente>(null, 'Erro ao cadastrar o paciente'))
    );
  }

  getPacientes(): Observable<Paciente[]>{
    return this.http.get<Paciente[]>(environment.URLBASE + '/paciente/buscarPacientes').pipe(
      catchError(this.errorHandler<Paciente[]>([], 'Erro ao buscar os pacientes'))
    );
  }

  getPacientePorId(id:String): Observable<Paciente>{
    return this.http.get<Paciente>(environment.URLBASE + '/paciente/buscarPaciente/:id').pipe(
      catchError(this.errorHandler<Paciente>(null, `Erro ao buscar o paciente ${id}`))
    );
  }

  patchPaciente(id:String, paciente:Object): Observable<Paciente>{
    return this.http.patch<Paciente>(environment.URLBASE + '/paciente/atualizarPaciente/:id' + id, paciente).pipe(
      catchError(this.errorHandler<Paciente>(null, 'Erro ao atualizar o usuário'))
    );
  }

  deletePaciente(id:String): Observable<null>{
    return this.http.delete<null>(environment.URLBASE + '/paciente/deletarPaciente' + id).pipe(
      catchError(this.errorHandler<null>(null, 'Erro ao deletar o usuário'))
    );
  }

  errorHandler<T>(resultado?: T, mensagem:string = 'Erro'){
    return (erro: HttpErrorResponse): Observable<T> => {
      if(erro.error instanceof ErrorEvent){
        console.log(`${mensagem} ${erro.error.message}`);
        return throwError(`${mensagem}: ${erro.error.message}`);
      }else{
        console.log(`${mensagem} - Status: ${erro.status}`);
        return of(resultado as T);
      }
    }
  }
}