import { Component, OnInit } from '@angular/core';
import { GoogleChartComponent } from 'angular-google-charts';

@Component({
  selector: 'app-health-data',
  templateUrl: './health-data.component.html',
  styleUrls: ['./health-data.component.css']
})
export class HealthDataComponent implements OnInit {

   type = 'LineChart';
   data = [
      ['Junho', 47],
      ['Julho', 117],
      ['Agosto', 416],

   ];
   options = {    
   };
   width = 600;
   height = 300;

   type_leitos = 'LineChart';
   data_leitos = [
      ['Junho', 45],
      ['Julho', 165],
      ['Agosto', 324],
   ];
   options_leitos = {    
   };
   width_leitos = 600;
   height_leitos = 300;

  constructor() { }

  ngOnInit(): void {

  }
}

