import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Unidade } from '../entidades/unidade';
import { Observable, throwError, of } from 'rxjs';
import { environment } from '../environments/environment';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UnidadeService {

  constructor(
    private http: HttpClient
  ) { }

  getUnidades(): Observable<Unidade[]>{
    return this.http.get<Unidade[]>(environment.URLBASE + '/unidade/buscarUnidades').pipe(
      catchError(this.errorHandler<Unidade[]>([], 'Erro ao buscar os unidades')));
  }

  getUnidadesPorId(id: string): Observable<Unidade>{
    return this.http.get<Unidade>(environment.URLBASE + '/unidade/buscarUnidade/' + id).pipe(
      catchError(this.errorHandler<Unidade>(null, `Erro ao buscar o unidade ${id}`)));
  }

  getUnidadesPorNome(nome: string): Observable<Unidade[]>{
    return this.http.get<Unidade[]>(environment.URLBASE + '/unidade/buscarUnidadeNome/' + nome).pipe(
      catchError(this.errorHandler<Unidade[]>([], `Erro ao buscar o unidade ${nome}`)));
  }

  postUnidade(unidade: Unidade): Observable<Unidade>{
    return this.http.post<Unidade>(environment.URLBASE + '/unidade/adicionarUnidade', unidade).pipe(
      catchError(this.errorHandler<Unidade>(null, 'Erro ao cadastrar unidade')));
  }

  patchUnidade(id: string , unidade: Unidade): Observable<Unidade>{
    return this.http.patch<Unidade>(environment.URLBASE + '/unidade/atualizarUnidade/' + id , unidade).pipe(
      catchError(this.errorHandler<Unidade>(null, 'Erro ao atualizar unidade')));
  }

  deleteUnidade(id: string): Observable<Unidade>{
    return this.http.delete<Unidade>(environment.URLBASE + '/unidade/deletarUnidade/' + id).pipe(
      catchError(this.errorHandler<null>(null, 'Erro ao deletar unidade')));
  }


  // tslint:disable-next-line: typedef
  errorHandler<T>(resultado?: T, mensagem: string = 'Erro'){
    return (erro: HttpErrorResponse): Observable<T> => {
      if (erro.error instanceof ErrorEvent){
        console.log(`${mensagem} ${erro.error.message}`);
        return throwError(`${mensagem}: ${erro.error.message}`);
      }else{
        console.log(`${mensagem} - Status: ${erro.status}`);
        return of(resultado as T);
      }
    };
  }
}
