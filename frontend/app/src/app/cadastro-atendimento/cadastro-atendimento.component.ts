import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PacienteService } from '../paciente.service';

interface Gender {
  value: string;
  viewValue: string;
}

interface Contagio {
  value: boolean;
  viewValue: string;
}

@Component({
  selector: 'app-cadastro-atendimento',
  templateUrl: './cadastro-atendimento.component.html',
  styleUrls: ['./cadastro-atendimento.component.css']
})
export class CadastroAtendimentoComponent implements OnInit {

  registerForm: FormGroup;
  selectedGender: string;
  selectedContagio: boolean;

  genders: Gender[] = [
    {value: 'masculino', viewValue: 'Masculino'},
    {value: 'feminino', viewValue: 'Feminino'}
  ];
  contagios: Contagio[] = [
    {value: true, viewValue: 'Positivo'},
    {value: false, viewValue: 'Negativo'}
  ];
 

  constructor(
    private pacienteService: PacienteService,
    private formBuilder: FormBuilder
  ) {
    this.registerForm = this.formBuilder.group({
      nome: ['', Validators.required],
      idade: ['', Validators.required],
      sexo: ['', Validators.required],
      contagio: ['', Validators.required],
      resultadoteste1: [''], 
      resultadoteste2: [''],
      horario_atendimento: ['', Validators.required],
      horario_saida : ['', Validators.required],
      informacoes_adicionais: ['', Validators.required]
    })
   }

  ngOnInit(): void {
  }

  onSubmit(atendimentoData) {
    this.pacienteService.postPaciente({
      nome: atendimentoData.nome,
      idade: atendimentoData.idade,
      sexo: atendimentoData.sexo,
      contagio: atendimentoData.contagio,
      resultadoteste1: atendimentoData.resultadoteste1,
      resultadoteste2: atendimentoData.resultadoteste2,
      horario_atendimento: atendimentoData.horario_atendimento,
      horario_saida: atendimentoData.horario_saida,
      informacoes_adicionais: atendimentoData.informacoes_adicionais,
    }).subscribe({
      next: console.log,
      error: console.warn
    });

    this.registerForm.reset();
    window.alert("Atendimento registrado!");

  }
}
